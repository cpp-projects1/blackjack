#ifndef deck
#define deck
#include <iostream>
#include <string>
#include <time.h>
#include <unistd.h>
using namespace std;

class Deck {
	public:
		int deal() {//deal cards
			int cards[] = {11, 11, 11, 11, 2, 2, 2, 2, 3, 3, 3, 3,
			4, 4, 4, 4, 5, 5, 5, 5, 6, 6, 6, 6, 7, 7, 7, 7, 
			8, 8, 8, 8, 9, 9, 9, 9, 10, 10, 10, 10, 10, 10, 10,
		       	10, 10, 10, 10, 10, 10, 10, 10, 11 };
			srand (time (NULL));
			int card;
			card = rand() % 52;
			sleep(1);
			return cards[card];
	
		};
			
		int hit_or_stay(int score) { //lets player decied to hit or stay 
		
			
			int choice;
			cout << ("Press 1 for hit, 2 for stay: ") << endl;
			cin >> choice;
			if (choice == 1) {

				int hit = deal();
				return hit;
						
				} else { 
				
					return 0;
			
			}
		};
		
		string check_score(int score){ //checks if player busted
		
			if (score > 21) {
			
				string str = "You Busted!!";
				return str;
			
			} else { string str = " "; return str;}
			
		};
		int dealer_hit(int score) { //decides if dealer should hit
		
			if (score < 17) {
			
				int hit = deal();
				return hit;	
			} else { return 0; }

		};

		string dealer_bust(int score){ //checks if dealer busted
		
			if (score > 21){
						
				string str = "Dealer Busted, you win!!";
				return str;
			
			} else {string str = "Dealer didn't bust."; return str;}

		};

		int ace_eleven_to_one(int score){ // offers player ability to change ace to 1
		
			int choice;	
			cout << "Change Ace to count as 1? 1 for yes 2 for no: " << endl;
			cin >> choice;
			
			if (choice == 1){
			
				score = score - 10;
			
			} 
		
			return score;	
		};
		
		int dealer_ace_eleven_to_one(int score){ // offers player ability to change ace to 1
		
			score = score - 10;

			return score;	
		};

};

#endif
