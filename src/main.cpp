#pragma once
#include "deck.hh"
#include <stdio.h>
int main() {

	Deck dealer;
	Deck player;

	int dealer_card1 = dealer.deal();
	int dealer_card2 = dealer.deal();
	int player_card1 = player.deal();
	int player_card2 = player.deal();
	cout << ("Dealer hand: ") << ("*Hiden Card*")<< (", ") << dealer_card2 << endl;
	cout << ("Your hand: ") << player_card1 << (", ") << player_card2 << endl;

	int dealer_score = dealer_card1 + dealer_card2;
	int player_score = player_card1 + player_card2;

	int player_pass = 0;
	int dealer_pass = 0;
	int again = 0;

	do { // do loop to control number of passes
		
		int hit = player.hit_or_stay(player_score);	
		cout << "Your hand is now: " << player_score << " + " << hit << endl;
		player_score = player_score + hit;

		if (player_card1 == 11 or player_card2 == 11 or hit == 11){
		
			player_score = player.ace_eleven_to_one(player_score);
		
		}
		
		cout << player_card1 << " " << player_card2 << " " << hit << endl;
		cout << player_score << endl;
			
		string player_bust = player.check_score(player_score);
		string player_bust_check = "You Busted!!";
		cout << player_bust << endl;

		if (player_bust == player_bust_check) { // checks if player, busted, ends game if true
			player_pass = 2;
			dealer_pass = 2;

		}
		else {	
			if (hit == 0){ // increases player pass if player stays
				player_pass++;
			}	

			string bust_check = "Dealer Busted, you win!!";

			int dealer_hit = dealer.dealer_hit(dealer_score);
			dealer_score = dealer_score + dealer_hit;
			if (dealer_score > 21){ // checks if dealer can change Ace to 11	
				if (dealer_card1 == 11 or dealer_card2 == 11 or dealer_hit == 11){
				
					dealer_score = dealer.dealer_ace_eleven_to_one(dealer_score);
				
				}
			}
			string bust = dealer.dealer_bust(dealer_score);
			
			if (bust == bust_check) {
				
				cout << bust << endl;
				player_pass = 2;
				dealer_pass = 2;
			
			} 
			
			if(dealer_hit == 0){ // increases dealer pass if dealer stays
				dealer_pass++;
			
			}
			printf("Dealer hand is: *Facedown card*, "); cout << dealer_card1 << ", "  << dealer_card2 << ", " << dealer_hit << endl;
		}

	} while (player_pass < 2 or dealer_pass < 2 ); 
	
	printf("Your score was: %d\n", player_score);
	printf("Dealer score was: %d\n", dealer_score);
	printf("Play again? 1 for yes, 2 for no: ");
	cin >> again;
	if (again == 1){
	
		main();
	
	} else { printf("Goodbye!"); cout << "\n";}	
	return 0;	
		

}
